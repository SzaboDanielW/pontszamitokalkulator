<?php

class Calculator{

    final function minRequirementsMet($exampleData, $mustHaveSubjects){
        $length = count($mustHaveSubjects);
        $requiredNumber = 0;
        foreach($exampleData as $element => $element_value){
            if($element == 'erettsegi-eredmenyek'){
                foreach($element_value as $subjects){
                    $minPoints = (str_replace('%', '', $subjects['eredmeny']));
                    if($minPoints <= 20){
                        echo 'hiba, nem lehetséges a pontszámítás a '.$subjects['nev'].' tárgyból elért 20% alatti eredmény miatt';
                        return 1;
                    }
                    for($i = 0; $i < $length; $i++){
                        if($subjects['nev'] == $mustHaveSubjects[$i]){
                            $requiredNumber += 1;
                        } 
                    }
                }
            }
        }
        if($requiredNumber == 3){
            return 0;
        }
        else{
            echo 'hiba, nem lehetséges a pontszámítás a kötelező érettségi tárgyak hiánya miatt';
            return 2;
        }
    }

    final function whichMajor($exampleData){
        $major = [];
        foreach($exampleData as $element => $element_value){
            if($element == 'valasztott-szak'){
                foreach($element_value as $variables){
                    array_push($major, $variables);
                }
            }
        }
        return $major;
    }

    final function findRequiredSubject($exampleData, $requiredSubject, $isAdvanced){
        foreach($exampleData as $element => $element_value){
            if($element == 'erettsegi-eredmenyek'){
                foreach($element_value as $subjects){
                    if($subjects['nev'] == $requiredSubject){
                        if(($subjects['tipus'] == 'közép') && $isAdvanced == 1){
                            echo 'hiba, nem lehetséges a pontszámítás, mert '.$subjects['nev'].' tantárgyból emelt érettségi szükséges a választott szakra';
                            return -1;
                        }
                        if((str_replace('%','',$subjects['eredmeny'])) > 20){
                            $num = str_replace('%','',$subjects['eredmeny']);
                            return $num;
                        }
                    }
                }
            }
        }
        return -1;
    }

    final function findBestOptionalSubject($exampleData, $optionalSubject){
        $length = count($optionalSubject);
        $bestScore = 0;
        $requiredOptionalsCount = 0;
        foreach($exampleData as $element => $element_value){
            if($element == 'erettsegi-eredmenyek'){
                foreach($element_value as $subjects){
                    foreach ($optionalSubject as $opt){
                        if($opt == $subjects['nev']){
                            $requiredOptionalsCount += 1;
                        }
                    }
                    for($i = 0; $i < $length; $i++){
                        $currentScore = str_replace('%','',$subjects['eredmeny']);
                        if($subjects['nev'] == $optionalSubject[$i] && $currentScore > $bestScore){
                            $bestScore = str_replace('%','',$subjects['eredmeny']);
                        }
                    }
                }
            }
        }
        if($requiredOptionalsCount <= 0){
            echo 'hiba, nem lehetséges a pontszámítás a kötelezően választható tárgy(ak) hiánya miatt';
            return -2;
        }
        return $bestScore;
    }

    final function calculateExtraPoints($exampleData){
        $advancedLanguageName = '';
        $lang = 'nyelv';
        $extraPoints = 0;
        foreach($exampleData as $element => $element_value){
            if($element == 'erettsegi-eredmenyek'){
                foreach($element_value as $advancedLevel){
                    if($advancedLevel['tipus'] == 'emelt'){
                        if((strpos($advancedLevel['nev'], $lang) !== false) && $advancedLevel['nev'] !== 'magyar nyelv és irodalom'){
                            $advancedLanguageName = str_replace(" nyelv", "", $advancedLevel['nev']);
                        }
                        $extraPoints += 50;
                    }
                }
            }
            if($element == 'tobbletpontok'){
                $c1Languagues = [];
                $b2Languages = [];
                foreach($element_value as $languagesKnown){
                    if($languagesKnown['tipus'] == 'C1' && $advancedLanguageName !== $languagesKnown['nyelv']){
                        array_push($c1Languagues, $languagesKnown['nyelv']);
                        $extraPoints += 40;
                    }
                    if($languagesKnown['tipus'] == 'B2' && $advancedLanguageName !== $languagesKnown['nyelv']){
                        array_push($b2Languages, $languagesKnown['nyelv']);
                        $extraPoints += 28;
                    }
                }
                foreach ($b2Languages as $b2){
                    foreach ($c1Languagues as $c1){
                        if ($b2 == $c1){
                            $extraPoints -= 28;
                        }
                    }
                }
            }
        }
        if ($extraPoints > 100){
            $extraPoints = 100;
        }
        return $extraPoints;
    }

    final function calculateFinalPoints($valueRequired, $ValueOptional, $extraPoints){
        if($valueRequired == -1){
            return null;
        }
        $basePoint = (($valueRequired + $ValueOptional) * 2);
        echo ($basePoint + $extraPoints).' ('.$basePoint.' alappont + '.$extraPoints.' többletpont)';
        return ($basePoint + $extraPoints);
    }
}
?>