<?php
require_once 'input.php';
require_once 'majors-class.php';
require_once 'calculator-class.php';

$calculator = new Calculator();
$pti = new Majors('ELTE', 'IK', 'Programtervező informatikus', 'matematika', 0, ['biológia', 'fizika', 'informatika', 'kémia']);
$ang = new Majors('PPKE', 'BTK', 'Anglisztika', 'angol nyelv', 1, ['francia', 'német', 'olasz', 'orosz', 'spanyol', 'történelem']);
$mustHaveSubjects = ['magyar nyelv és irodalom', 'történelem', 'matematika'];

global $exampleData6;
$Student = $exampleData6;

if ($calculator->minRequirementsMet($Student, $mustHaveSubjects) == 0){
    $chosenMajor = $calculator->whichMajor($Student);

    if($chosenMajor[0] == $pti->get_universityName($pti) && $chosenMajor[1] == $pti->get_departmentName($pti) && $chosenMajor[2] == $pti->get_majorName($pti)){
        if(($calculator->findBestOptionalSubject($Student, $pti->get_optionalSubjectNames($pti))) <= 0){
            return 1;
        }
        $calculator->calculateFinalPoints($calculator->findRequiredSubject($Student, $pti->get_requiredSubjectName($pti),
            $pti->get_AdvancedSubject($pti)), $calculator->findBestOptionalSubject($Student, $pti->get_optionalSubjectNames($pti)),
            $calculator->calculateExtraPoints($Student));
    }

    if($chosenMajor[0] == $ang->get_universityName($ang) && $chosenMajor[1] == $ang->get_departmentName($ang) && $chosenMajor[2] == $ang->get_majorName($ang)){
        if(($calculator->findBestOptionalSubject($Student, $ang->get_optionalSubjectNames($ang))) <= 0){
            return 1;
        }
        $calculator->calculateFinalPoints($calculator->findRequiredSubject($Student, $ang->get_requiredSubjectName($ang),
            $ang->get_AdvancedSubject($ang)), $calculator->findBestOptionalSubject($Student, $ang->get_optionalSubjectNames($ang)),
            $calculator->calculateExtraPoints($Student));
    }
}
?>