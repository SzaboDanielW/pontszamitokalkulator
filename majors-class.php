<?php

class Majors{
    private $universityName;
    private $departmentName;
    private $majorName;
    private $requiredSubjectName;
    private $AdvancedSubject;
    private $optionalSubjectNames;

    function get_universityName($universityName){
        return $this->universityName;
    }

    function get_departmentName($departmentName){
        return $this->departmentName;
    }

    function get_majorName($majorName){
        return $this->majorName;
    }

    function get_requiredSubjectName($requiredSubjectName){
        return $this->requiredSubjectName;
    }

    function get_AdvancedSubject($AdvancedSubject){
        return $this->AdvancedSubject;
    }

    function get_optionalSubjectNames($optionalSubjectNames){
        return $this->optionalSubjectNames;
    }

    function __construct($universityName, $departmentName, $majorName, $requiredSubjectName, $AdvancedSubject, $optionalSubjectNames){
        $this->universityName       = $universityName;
        $this->departmentName       = $departmentName;
        $this->majorName            = $majorName;
        $this->requiredSubjectName  = $requiredSubjectName;
        $this->AdvancedSubject      = $AdvancedSubject;
        $this->optionalSubjectNames = $optionalSubjectNames;
    }
}
?>